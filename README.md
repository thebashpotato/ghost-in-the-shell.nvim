<div align="center">
  <h1>neovim.config</h1>
  <img src="./assets/neovim-config-1.png">
</div>
<br>
<div align="center">
  <img alt="Crates.io" src="https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square">
  <br>
  <p>A modern <b>neovim</b> config  written in Lua for low level programmers</p>
</div>

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

There isn't much to install, just copy the `nvim` directory to `~/.config/nvim`.
But for the sake of commands..

```
# clone the repo to home
git clone --depth 1 git@gitlab.com:thebashpotato/ghost-in-the-shell.nvim.git ~/

# make a soft link to the config
ln -s $(pwd)/neovim.config/nvim ~/.config/nvim

# open nvim and let it set up
nvim
```

## Usage

TODO: Fill out docs later

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

AGPLv3 © 2022 Matt Williams
